package com.example.validator.json.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StreetRequest {

    @NotNull
    @Min(1)
    @Max(100)
    private Long key;
}
