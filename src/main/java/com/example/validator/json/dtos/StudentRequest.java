package com.example.validator.json.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.validation.executable.ValidateOnExecution;
import java.util.List;

@Getter
@Setter
public class StudentRequest {
    @NotNull
    @Min(10)
    @Max(100)
    private Long key;

    @NotBlank
    @Pattern(regexp ="name1|name2|name3")
    private String name;

    @Min(1)
    @Max(100)
    private Long age;

    @Size(min = 1,max = 3)
    private List<Long> keys;

    @NotNull(message = "")
    @Valid
    private AddressRequest address;


}
