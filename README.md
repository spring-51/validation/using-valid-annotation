## Validating Request Using @Valid annotation

### Step to start app
```s
s1 -  git clone
s2 - cd 
s3 - Start Spring boot app
s4 -  POST Access {baseUrl}/students
```

### Endpoint
```ep
POST  {baseUrl}/students
```

### RequestBody

#### Invalid

```body
{
    "key": null,
    "name":"name4",
    "age":111,
    "keys":[],
    "address":{
        "key": null,
        "street":{
            "key": null
        }
    }
}

```

#### Valid

```body
{
    "key": 11,
    "name":"name3",
    "age":11,
    "keys":[1],
    "address":{
        "key": 11,
        "street":{
            "key": 11
        }
    }
}

```